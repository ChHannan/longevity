class Event {
  final String title;
  final String subtitle;
  final String? image;

  const Event({
    required this.title,
    required this.subtitle,
    this.image,
  });
}
