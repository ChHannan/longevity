import 'package:flutter/widgets.dart';

class LongevityIcons {
  LongevityIcons._();

  static const _kFontFam = 'LongevityIcons';
  static const String? _kFontPkg = null;

  static const IconData chart =
      IconData(0xe83a, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData setting =
      IconData(0xe83b, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData heartbeat =
      IconData(0xe83c, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData pill =
      IconData(0xe83d, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData medicalFile =
      IconData(0xe83e, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData stethoscope =
      IconData(0xe83f, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData prescriptions =
      IconData(0xe840, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData handWatch =
      IconData(0xe841, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData fillPill =
      IconData(0xe845, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData arrow =
      IconData(0xe846, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}
