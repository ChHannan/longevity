import 'package:flutter/material.dart';

extension WidgetExtension on Widget {
  Widget get pad => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: this,
      );
}



