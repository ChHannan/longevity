import 'dart:ui';

const kAquaColor = Color(0xFF20EAC1);
const kLightAquaColor = Color(0xFF1EDFB8);
const kBlackColor = Color(0xFF222635);
const kAmberColor = Color(0xFFF7A340);
const kBlueColor = Color(0xFF20BAEA);
const kShadowColor = Color(0x1F4a555f);
const kBorderColor = Color(0xFFF5FAF9);
const kMarkerColor =  Color(0xFFFF9900);
const kLightColor =  Color(0xFFB6B6C3);

final kTodayInUTC = DateTime.utc(2022, DateTime.now().month,  DateTime.now().day);