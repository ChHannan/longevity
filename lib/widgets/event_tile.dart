import 'package:flutter/material.dart';
import 'package:longevity/utils/custom_icons.dart';
import 'package:longevity/models/event.dart';
import 'package:longevity/utils/constants.dart';

class EventTile extends StatelessWidget {
  final Event event;

  const EventTile({
    Key? key,
    required this.event,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4.0),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
            color: const Color(0xFFF5FAF9),
          ),
          borderRadius: BorderRadius.circular(8),
        ),
        child: ListTile(
          onTap: () {},
          title: Text(
            event.title,
            style: const TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.w400,
              color: Color(0xFF222635),
            ),
          ),
          subtitle: Text(
            event.subtitle,
            style: const TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: kLightColor,
            ),
          ),
          leading: event.image == null
              ? Container(
                  width: 32,
                  height: 32,
                  padding: const EdgeInsets.all(8),
                  decoration: BoxDecoration(
                    color: const Color(0xFFF5FAF9),
                    borderRadius: BorderRadius.circular(16.0),
                  ),
                  child: const Icon(
                    LongevityIcons.fillPill,
                    size: 16,
                    color: Colors.black,
                  ),
                )
              : CircleAvatar(
                  radius: 16,
                  backgroundImage: AssetImage(event.image!),
                ),
          trailing: const Icon(
            Icons.arrow_forward_ios,
            color: kAquaColor,
            size: 16,
          ),
        ),
      ),
    );
  }
}
