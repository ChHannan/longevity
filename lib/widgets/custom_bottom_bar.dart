import 'package:flutter/material.dart';
import 'package:longevity/utils/custom_icons.dart';
import 'package:longevity/utils/constants.dart';

class CustomBottomBar extends StatelessWidget {
  final int index;
  final Function(int) onTap;

   const CustomBottomBar({
    Key? key,
    required this.index,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(24),
          topRight: Radius.circular(23),
        ),
        boxShadow: [
          BoxShadow(
            color: kShadowColor,
            blurRadius: 12,
            offset: Offset(0, 2),
          ),
        ],
      ),
      child: ClipRRect(
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(24),
          topRight: Radius.circular(24),
        ),
        child: BottomNavigationBar(
          items: const [
            BottomNavigationBarItem(
              icon: Icon(LongevityIcons.chart),
              label: 'Dashboard',
            ),
            BottomNavigationBarItem(
              icon: Icon(LongevityIcons.pill),
              label: 'Prescriptions',
            ),
            BottomNavigationBarItem(
              icon: Icon(LongevityIcons.heartbeat),
              label: 'History',
            ),
            BottomNavigationBarItem(
              icon: Icon(LongevityIcons.setting),
              label: 'Settings',
            ),
          ],
          currentIndex: index,
          selectedItemColor: kAquaColor,
          unselectedItemColor: kLightColor,
          showUnselectedLabels: true,
          backgroundColor: Colors.white,
          type: BottomNavigationBarType.fixed,
          onTap: onTap,
        ),
      ),
    );
  }
}
