import 'package:flutter/material.dart';
import 'package:longevity/utils/constants.dart';
import 'package:longevity/widgets/vital_signs_indicator.dart';

class CustomAppBar extends StatefulWidget {
  const CustomAppBar({Key? key}) : super(key: key);

  @override
  State<CustomAppBar> createState() => _CustomAppBarState();
}

class _CustomAppBarState extends State<CustomAppBar> {
  late double _initialHeight;
  late double _expandedHeight;
  bool isExpanded = false;

  @override
  void initState() {
    super.initState();
    _initialHeight = 305;
    _expandedHeight = 534;
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 500),
      curve: Curves.ease,
      height: isExpanded ? _expandedHeight : _initialHeight,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          colors: [
            kAquaColor,
            kLightAquaColor,
          ],
        ),
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(24),
          bottomRight: Radius.circular(24),
        ),
      ),
      child: Stack(
        clipBehavior: Clip.none,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 12),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Text(
                      'Welcome, Joe Doe',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 24,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    CircleAvatar(
                      backgroundColor: Colors.grey,
                      backgroundImage: AssetImage(
                        'assets/images/doctor.png',
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 16,
                ),
                Expanded(
                  child: ListView(
                    physics: const NeverScrollableScrollPhysics(),
                    children: const [
                      CustomGridLayout(
                        firstChild: VitalSignIndicator(
                          signValue: 81,
                          signUnit: 'BPM',
                          signName: 'Heart Rate  ·  10s ago',
                          backgroundColor: kBlackColor,
                          signNameColor: Colors.white,
                          signValueColor: Colors.white,
                          signUnitColor: Colors.white,
                        ),
                        secondChild: VitalSignIndicator(
                          signValue: 63.1,
                          signUnit: 'kg',
                          signName: 'Weight',
                        ),
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      CustomGridLayout(
                        firstChild: VitalSignIndicator(
                          signValue: 98.6,
                          signUnit: '\u00B0C',
                          signName: 'Temperature',
                        ),
                        secondChild: VitalSignIndicator(
                          signValue: 140,
                          signUnit: 'mmHg',
                          signName: 'Blood Pressure',
                          signValueColor: kAmberColor,
                        ),
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      CustomGridLayout(
                        firstChild: VitalSignIndicator(
                          signValue: 100,
                          signUnit: '',
                          signName: 'Pulse',
                        ),
                        secondChild: VitalSignIndicator(
                          signValue: 24.2,
                          signUnit: '',
                          signName: 'BMI',
                        ),
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      CustomGridLayout(
                        firstChild: VitalSignIndicator(
                          signValue: 23.1,
                          signUnit: '%',
                          signName: 'Body Fat',
                          signValueColor: kAmberColor,
                        ),
                        secondChild: VitalSignIndicator(
                          signValue: 1980,
                          signUnit: 'kcal',
                          signName: 'BMR',
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Positioned.fill(
            bottom: -15,
            child: Align(
              alignment: Alignment.bottomCenter,
              child: GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {
                  setState(() {
                    isExpanded = !isExpanded;
                  });
                },
                child: Container(
                  height: 28,
                  width: 28,
                  decoration: BoxDecoration(
                      border: Border.all(color: kBorderColor),
                      color: Colors.white,
                      borderRadius: const BorderRadius.all(
                        Radius.circular(28),
                      ),
                      boxShadow: const [
                        BoxShadow(
                          color: kShadowColor,
                          blurRadius: 12,
                          offset: Offset(0, 2), // changes position of shadow
                        ),
                      ]),
                  child: Center(
                    child: Icon(
                      isExpanded
                          ? Icons.keyboard_arrow_up_outlined
                          : Icons.keyboard_arrow_down_outlined,
                      color: kAquaColor,
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

class CustomGridLayout extends StatelessWidget {
  final Widget firstChild;
  final Widget secondChild;

  const CustomGridLayout({
    Key? key,
    required this.firstChild,
    required this.secondChild,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: firstChild,
        ),
        const SizedBox(
          width: 12,
        ),
        Expanded(
          child: secondChild,
        ),
      ],
    );
  }
}
