import 'package:flutter/material.dart';
import 'package:longevity/utils/custom_icons.dart';
import 'package:longevity/utils/constants.dart';

class ActionTile extends StatelessWidget {
  const ActionTile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: const [
        ActionItem(
          label: 'Find a\nSpecialist',
          icon: LongevityIcons.medicalFile,
          backgroundColor: kBlackColor,
        ),
        ActionItem(
          label: 'Log\nSymptoms',
          icon: LongevityIcons.stethoscope,
          backgroundColor: kAmberColor,
        ),
        ActionItem(
          label: 'View\nPrescriptions',
          icon: LongevityIcons.prescriptions,
          backgroundColor: kAquaColor,
        ),
        ActionItem(
          label: 'Connect\nDevices',
          icon: LongevityIcons.handWatch,
          backgroundColor: kBlueColor,
        ),
      ],
    );
  }
}

class ActionItem extends StatelessWidget {
  final String label;
  final IconData icon;
  final Color backgroundColor;

  const ActionItem({
    Key? key,
    required this.label,
    required this.icon,
    required this.backgroundColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 64,
          height: 64,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(32),
            color: backgroundColor,
          ),
          child: Icon(
            icon,
            color: Colors.white,
            size: 32,
          ),
        ),
        const SizedBox(
          height: 8,
        ),
        Text(
          label,
          textAlign: TextAlign.center,
          style: const TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.w400,
            letterSpacing: 0.32,
          ),
        ),
      ],
    );
  }
}
