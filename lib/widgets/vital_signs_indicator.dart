import 'package:flutter/material.dart';
import 'package:longevity/utils/constants.dart';

class VitalSignIndicator extends StatelessWidget {
  final num signValue;
  final String signUnit;
  final String signName;
  final Color backgroundColor;
  final Color signValueColor;
  final Color signUnitColor;
  final Color signNameColor;

  const VitalSignIndicator({
    Key? key,
    required this.signValue,
    required this.signUnit,
    required this.signName,
    this.backgroundColor = Colors.white,
    this.signValueColor = kBlackColor,
    this.signUnitColor = kLightColor,
    this.signNameColor = kLightColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 103,
      padding: const EdgeInsets.symmetric(
        vertical: 12,
        horizontal: 16,
      ),
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: BorderRadius.circular(12.0),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.baseline,
            textBaseline: TextBaseline.alphabetic,
            children: [
              Text(
                signValue.toString(),
                style: TextStyle(
                  color: signValueColor,
                  fontSize: 40,
                  fontWeight: FontWeight.w500,
                ),
              ),
              const SizedBox(
                width: 8,
              ),
              Text(
                signUnit,
                style: TextStyle(
                  color: signUnitColor,
                  fontSize: 20,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ],
          ),
          Text(
            signName,
            style: TextStyle(
              color: signNameColor,
              fontSize: 12,
              fontWeight: FontWeight.w400,
              letterSpacing: 0.32,
            ),
          ),
        ],
      ),
    );
  }
}
