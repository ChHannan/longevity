import 'package:flutter/material.dart';
import 'package:longevity/models/event.dart';
import 'package:longevity/utils/constants.dart';
import 'package:table_calendar/table_calendar.dart';

class Calendar extends StatelessWidget {
  final List<Event> Function(DateTime)? events;

  const Calendar({
    Key? key,
    required this.events,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TableCalendar<Event>(
      startingDayOfWeek: StartingDayOfWeek.monday,
      firstDay: DateTime.utc(2010, 10, 16),
      lastDay: DateTime.utc(2030, 3, 14),
      focusedDay: DateTime.now().toUtc(),
      calendarFormat: CalendarFormat.week,
      headerVisible: false,
      rowHeight: 64,
      eventLoader: events,
      calendarBuilders: CalendarBuilders(
        defaultBuilder: (context, date, events) => Container(
          width: 32,
          height: 48,
          margin: const EdgeInsets.only(bottom: 12),
          alignment: Alignment.center,
          child: Text(
            date.day.toString(),
            style: const TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        todayBuilder: (context, date, events) => Container(
          width: 32,
          height: 48,
          margin: const EdgeInsets.only(bottom: 12),
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: kBlackColor,
            borderRadius: BorderRadius.circular(17.0),
          ),
          child: Text(
            date.day.toString(),
            style: const TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        singleMarkerBuilder: (context, date, events) {
          return Column(
            children: [
              const SizedBox(
                height: 8,
              ),
              Container(
                width: 6,
                height: 6,
                margin: const EdgeInsets.symmetric(horizontal: 3),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: kMarkerColor,
                  borderRadius: BorderRadius.circular(3),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
