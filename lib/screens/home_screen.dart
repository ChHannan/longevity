import 'package:flutter/material.dart';
import 'package:longevity/utils/constants.dart';
import 'package:longevity/utils/extensions.dart';
import 'package:longevity/models/event.dart';
import 'package:longevity/widgets/action_tile.dart';
import 'package:longevity/widgets/calendar.dart';
import 'package:longevity/widgets/custom_app_bar.dart';
import 'package:longevity/widgets/custom_bottom_bar.dart';
import 'package:longevity/widgets/event_tile.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({
    super.key,
  });

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final Map<DateTime, List<Event>> _events = {
    kTodayInUTC: [
      const Event(title: 'Take Relaxon', subtitle: '50mg @ 6:00 PM'),
      const Event(
        title: 'Dr. John',
        subtitle: 'Routine check-up @ 12:00 PM',
        image: 'assets/images/doctor.png',
      ),
    ],
  };

  List<Event> _getEventsForDay(DateTime day) {
    return _events[day] ?? [];
  }

  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: CustomBottomBar(
          index: _selectedIndex,
          onTap: _onItemTapped,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              const CustomAppBar(),
              const SizedBox(height: 20.0),
              Column(
                children: [
                  const ActionTile(),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: const [
                      Text(
                        'Upcoming Activities',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      Text(
                        'View All',
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: kAquaColor,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Calendar(
                    events: _getEventsForDay,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  EventTile(
                    event: _events[kTodayInUTC]![0],
                  ),
                  EventTile(
                    event: _events[kTodayInUTC]![1],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                ],
              ).pad,
            ],
          ),
        ),
      ),
    );
  }
}
